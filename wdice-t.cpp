/* DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar
 *  14 rue de Plaisance, 75014 Paris, France
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include <string>
#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <math.h>
#include <iomanip>
#include <thread>
#include <vector>
using namespace std;


// Struct to store the results of the threaded rolls.
struct threadData {
  signed int iSumSuccesses = 0; // Sum of total success dice
  signed int iSumBotches = 0; // Sum of total fail dice
  signed int iSuccessRolls = 0; // Amount of success rolls
  signed int iZeroRolls = 0; // Amount of failed rolls
  signed int iBotchRolls = 0; // Amount of botched rolls
  signed int iHighestRollTimes = 0; // Amount of highest success
  signed int iLowestRollTimes = 0; // Amount of lowest botch
  signed int iPositiveResults[50] = {0}; // Array of successes
  signed int iNegativeResults[50] = {0}; // Array of botches
  signed int iTimesToRoll; // Times to roll
  signed char iHighestSuccess = 0; // Highest sucess
  signed char iLowestBotch = 0; // Lowest botch
  signed char iDice;
  signed char iDifficulty;
  bool bSpec = false;
};


// Global variables - The threads need to have access to this data.
std::vector<std::thread> vThreads;
std::vector<threadData> vThreadData; // Results of the threaded rolls.
signed int iTimesToRoll; // Times to roll
unsigned int iThreads;
signed char iDice; // Amount of dice
signed char iDifficulty; // Roll difficulty
bool bSpec = false; // Specialization roll?


// Thread payload. This is where the rolls happen.
bool runThread(int tNum)
{
  // Other thread-local variables
  signed int successes = 0;
  unsigned int seed = time(NULL)+tNum; // Store seed thread-locally.
  signed char iDiceLeft;
  signed char result;
  bool nobotch = false;

  // Store global data locally so we avoid shared access in the loop
  threadData myData;
  myData.iDice = iDice;
  myData.iDifficulty = iDifficulty;
  myData.iTimesToRoll = (signed int) floor(iTimesToRoll/iThreads);
  myData.bSpec = bSpec;

  // Per-roll loop
  while(myData.iTimesToRoll-- != 0)
  {

    successes = 0;
    nobotch = false;
    iDiceLeft = myData.iDice; // Amount of dice

    // Per-die loop
    while(iDiceLeft--)
    {
      // Get result of the die roll. rand_r over rand for thread local seed.
      result = ((int) (10.0 * (rand_r(&seed) / (RAND_MAX + 1.0)))) + 1;

      if(result >= myData.iDifficulty) // If result beats difficulty...
      {
        ++successes;
        nobotch = true;

        // if specalization-mode is on, give an additional die
        if(result == 10)
        {
          if(myData.bSpec)
            ++successes;
        }
      }
      else if(result == 1) // If result is 1, remove a success
        --successes;
    }

    if(successes > 0) // If more than one success...
    {
      ++myData.iPositiveResults[successes]; // Record amount in success array
      myData.iSumSuccesses += successes; // Add successes to total sum for stats
      ++myData.iSuccessRolls; // Increase successful rolls
      if(successes >= myData.iHighestSuccess)
      {
	      if(successes == myData.iHighestSuccess) // Successes matches current record
	        ++myData.iHighestRollTimes; // Increase amount of highest roll
				else // New high record
				{
	        myData.iHighestRollTimes = 1; // Reset amount and set highest success value
	        myData.iHighestSuccess = successes;
				}
      }
    }
		else if(!nobotch)
		{
	    if(successes < 0) // If we have a botch...
	    {
	      ++myData.iNegativeResults[abs(successes)]; // Record amount in botch array
	      myData.iSumBotches += successes; // Add 1's to total sum for stats
	      ++myData.iBotchRolls; // Increase botch rolls
	      if(successes < myData.iLowestBotch) // If we have a new low record..
	      {
	        myData.iLowestRollTimes = 1; // Reset amount and set lowest botch value
	        myData.iLowestBotch = successes;
	      }
	      else if(successes == myData.iLowestBotch)
	        ++myData.iLowestRollTimes; // Else increase amount of lowest rolls
	    }
	    else
	      ++myData.iZeroRolls; // Increase amount of failed (0) rolls
		}
	  else
	    ++myData.iZeroRolls; // Increase amount of failed (0) rolls
  }
  vThreadData.at(tNum) = myData;
  return 1;
}

// Main program
int main(int argc, char *argv[])
{
  // Store argument-specific values for thread access.
  iDice = max(min(atoi(argv[1]),35),1); // Amount of dice, max of 35
  iDifficulty = max(min(atoi(argv[2]),9),2); // Min diff 2, max diff 9
  iTimesToRoll = max(min(atoi(argv[3]),1000000000),10000); // Times to roll, max 1 billion. Also let's not roll less than 10000 times.
  if(argv[4])
    if(atoi(argv[4]) > 0)
      bSpec=true;


  // Determine max threads
  iThreads  = std::thread::hardware_concurrency();

  // Kick off threads...
  for(int i=0;i<iThreads;i++)
  {
    threadData t1d;
    vThreadData.push_back(t1d);
    thread t1(runThread,i);
    vThreads.push_back(std::move(t1));
  }

  // ..and wait for them to be done.
  for(int i=0;i<vThreads.size();i++)
    vThreads.at(i).join();

  // Variables needed after all rolls are done.
  signed int iSumSuccesses = 0; // Sum of total success dice
  signed int iSumBotches = 0; // Sum of total fail dice
  signed int iSuccessRolls = 0; // Amount of success rolls
  signed int iZeroRolls = 0; // Amount of failed rolls
  signed int iBotchRolls = 0; // Amount of botched rolls
  signed int iHighestRollTimes = 0; // Amount of highest success
  signed int iLowestRollTimes = 0; // Amount of lowest botch
  signed int iPositiveResults[50] = {0}; // Array of successes
  signed int iNegativeResults[50] = {0}; // Array of botches
  signed char iHighestSuccess = 0; // Highest sucess
  signed char iLowestBotch = 0; // Lowest botch

  // Grab all the results of the threads and merge them properly for display.
  signed int x;
  signed char y;
  for(x = 0;x < iThreads;x++)
  {
     iSuccessRolls += vThreadData.at(x).iSuccessRolls;
     iZeroRolls += vThreadData.at(x).iZeroRolls;
     iBotchRolls += vThreadData.at(x).iBotchRolls;
     iSumSuccesses += vThreadData.at(x).iSumSuccesses;
     iSumBotches += vThreadData.at(x).iSumBotches;

     if(vThreadData.at(x).iHighestSuccess >= iHighestSuccess)
     {
       if((vThreadData.at(x).iHighestSuccess > iHighestSuccess))
       {
         iHighestSuccess = vThreadData.at(x).iHighestSuccess;
         iHighestRollTimes = vThreadData.at(x).iHighestRollTimes;
       }
       else
       {
         iHighestRollTimes += vThreadData.at(x).iHighestRollTimes;
       }
     }

     if(vThreadData.at(x).iLowestBotch <= iLowestBotch)
     {
       if((vThreadData.at(x).iLowestBotch < iLowestBotch))
       {
         iLowestBotch = vThreadData.at(x).iLowestBotch;
         iLowestRollTimes = vThreadData.at(x).iLowestRollTimes;
       }
       else
       {
         iLowestRollTimes += vThreadData.at(x).iLowestRollTimes;
       }
     }
     for(y = 1;y < 50;y++)
     {
       iPositiveResults[y] += vThreadData.at(x).iPositiveResults[y];
       iNegativeResults[y] += vThreadData.at(x).iNegativeResults[y];
     }
  }

	// We're done. Let's output stuff. Amount of successes/fails/botches
  cout << "\nWon: \033[1;32m" << iSuccessRolls << "\033[0m  Fail: " << iZeroRolls << "  Botch: \033[1;31m" << iBotchRolls << "\033[0m" << endl;

	// Calculate percentages of successes/fails/botches
  cout << "Chance: Success -> ";
  if(!iSuccessRolls)
    cout << "\033[1;31m0\033[0m";
  else
    cout << "\033[1;32m" << setprecision(3) << (float(iSuccessRolls) / float(iTimesToRoll)) * 100.0;
  cout << "%\033[0m  Fail: -> ";
  if(!iZeroRolls)
    cout << "\033[1;32m0";
  else
    cout << setprecision(3) << (float(iZeroRolls) / float(iTimesToRoll)) * 100.0;
  cout <<  "%\033[0m  Botch: -> ";
  if(!iBotchRolls)
    cout << "\033[1;32m0%\033[0m" << endl;
  else
    cout << "\033[1;31m" << setprecision(3) << (float(iBotchRolls) / float(iTimesToRoll)) * 100.0 << "%\033[0m" << endl;

	// Output highest success and lowest botch, along with amounts
  cout << "Most successes: \033[1;32m" << (signed int) iHighestSuccess << "\033[0m (" << iHighestRollTimes << " times)  " << "Least successes: \033[1;31m" << (signed int) iLowestBotch << "\033[0m (" << iLowestRollTimes << " times)" << endl;

	// Output statistics about all rolled successes and botches
  signed int i;
  cout << "Successes:  ";
  for(i = 1;i < 50;i++)
  {
    if(iPositiveResults[i] > 0)
      cout << setprecision(3) << "\033[1;32m" << i << "\033[0m: " << iPositiveResults[i] << "(\033[1;32m" << (float(iPositiveResults[i]) / float(iTimesToRoll)) * 100.0 << "%\033[0m)" << "  ";
  }
  cout << endl;
  cout << "Botches:  ";
  for(i = 0;i < 50;i++)
  {
    if(iNegativeResults[i] > 0)
      cout << setprecision(3) << "\033[1;31m-" << i << "\033[0m: " << iNegativeResults[i] << "(\033[1;31m" << (float(iNegativeResults[i]) / float(iTimesToRoll)) * 100.0 << "%\033[0m)" << "  ";
  }
  cout << endl;

	// Output average success
  cout << "Average success: ";
  if(!iSuccessRolls)
    cout << "-" << endl;
  else
    cout << "\033[1;32m" << (float(iSumSuccesses) / float(iSuccessRolls)) << "\033[0m" << endl;

	// Output average botch
  cout << "Average botch: ";
  if(!iBotchRolls)
    cout << "-\n" << endl;
  else
    cout << "\033[1;31m" << (float(iSumBotches) / float(iBotchRolls)) << "\033[0m\n" << endl;

	return 0;
}
